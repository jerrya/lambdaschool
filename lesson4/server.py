from flask import Flask, render_template, request, jsonify
import sqlite3

app = Flask(__name__)

connection = sqlite3.connect('database.db')
connection.execute(
    "CREATE TABLE IF NOT EXISTS friends (name TEXT, age INTEGER)")
print('Table created successfully')
connection.close()


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/new-friend', methods=['POST'])
def newFriend():

    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    try:
        name = request.form['name']
        age = request.form['age']
        cursor.execute(
            'INSERT INTO friends (name, age) VALUES (?, ?)', (name, age))
        connection.commit()
        message = 'Record successfully added'
    except Exception as e:
        print("%s" % e)
        connection.rollback()
        message = "error in insert operation"
    finally:
        connection.close()
        return render_template('result.html', message=message)


@app.route('/friends')
def friends():
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute('select * from friends')
    friendsList = cursor.fetchall()
    connection.close()
    return jsonify(friendsList)


app.run(debug=True)
