from flask import Flask, render_template, request, jsonify
import sqlite3

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/new-post', methods=['POST'])
def newPost():

    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    author = request.form['author']
    title = request.form['title']
    body = request.form['body']
    likes = 0
    id = request.form['id']
    print(title)
    print(id)
    try:

        cursor.execute('INSERT INTO posts(author, title, body, likes, id)'
                       'VALUES (?, ?, ?, ?, ?)',
                       (author, title, body, likes, id))
        connection.commit()
        message = 'Record successfully added'
    except sqlite3.Error as e:
        print("%s %s" % (e, e.args))
        connection.rollback()
        message = "error in insert operation: %s: %s" % (e, e.args)
        raise e
    finally:
        connection.close()
        return render_template('result.html', message=message)


@app.route('/posts')
def getPosts():
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute('select * from posts')
    postsList = cursor.fetchall()
    connection.close()
    return jsonify(postsList)


@app.route('/like/<post_id>')
def likePost(post_id):
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()
    cursor.execute('update posts set likes=likes + 1 where id=?', (post_id))
    connection.commit()
    cursor.execute('select likes from posts where id=?', (post_id))
    likesList = cursor.fetchall()
    connection.close()
    likes = 0
    if likesList:
        likes = likesList[0][0]
    # return 'post ' + post_id + ' likes: ' + str(likes)
    return str(likes)


app.run(debug=True)
