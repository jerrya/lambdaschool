$(function() {
  populatePosts();
});
  
  
function populatePosts() {
  var template = $('#post-template').html();
  $.ajax({
  	url: '/posts'
  }).done(function(response) {
  	response.forEach(function(post) {
  	  var newPost = $(template).clone();
  	  $(newPost).find('.title').html(post[1]);
  	  $(newPost).find('.author').html(post[0]);
  	  $(newPost).find('.body').html(post[2]);
  	  $(newPost).find('.likes').html(post[3]);
  	  $(newPost).find('.hidden-id').html(post[4]);
  	  $(newPost).find('.like-button').on('click', incrementLike);
  	  $('#post-list').append(newPost);
  	});
  });
}
  
function incrementLike(event) {
  // console.log("------");
  // console.debug(event);
  // console.debug(this);
  // console.debug($(this));
  // console.debug($(this).parent());
  var likes = $(this).parent().find('.likes');
  var id = $(this).find('.hidden-id').html();
  $.ajax({
  	url: '/like/' + id
  }).done(function(response) {
  	// console.log('>>>response', response);
    // var h = $(this).find('.likes').html();
    // console.log('>>>html', h);
    // console.debug(this);
    // console.debug($(this));
    // console.debug($(this).parent());
    $(likes).html(response);
    // console.log("=====");
  });
} 
