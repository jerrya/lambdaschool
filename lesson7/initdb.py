import sqlite3

connection = sqlite3.connect('database.db')
print("db opened")
connection.execute(
    "CREATE TABLE IF NOT EXISTS posts ("
    "author TEXT,"
    "title TEXT,"
    "body TEXT,"
    "likes INTEGER,"
    "id INTEGER)")

print('Table created successfully')
connection.close()
