from flask import Flask, render_template, jsonify

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("home.html")


@app.route("/foods")
def foods():
    return jsonify(["a", "b", "c"])


@app.route("/about")
def about():
    return app.send_static_file("about.html")


@app.route("/contact")
def contact():
    return app.send_static_file("contact.html")


@app.route("/post/<postnum>")
def post(postnum):
    return ("This is post %s" % postnum)
