from flask import Flask, render_template, request, jsonify
import sqlite3

app = Flask(__name__)

connection = sqlite3.connect('database.db')
connection.execute(
    "CREATE TABLE IF NOT EXISTS books (name TEXT, author TEXT, "
    "yearpublished INTEGER, pages INTEGER)")
print('Table created successfully')
connection.close()


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/new-book', methods=['POST'])
def newBook():

    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    try:
        name = request.form['name']
        author = request.form['author']
        yearPublished = request.form['yearPublished']
        pages = request.form['pages']
        cursor.execute(
            'INSERT INTO books (name, author, yearPublished, pages) '
            'VALUES (?, ?, ?, ?)', (name, author, yearPublished, pages))
        connection.commit()
        message = 'Record successfully added'
    except sqlite3.Error as e:
        print("%s %s" % (e, e.args))
        connection.rollback()
        message = "error in insert operation: %s: %s" % (e, e.args)
        raise e
    finally:
        connection.close()
        return render_template('result.html', message=message)


@app.route('/books')
def books():
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute('select * from books')
    booksList = cursor.fetchall()
    connection.close()
    return jsonify(booksList)


app.run(debug=True)
