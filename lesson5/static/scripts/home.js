console.log("hello world!");

document.getElementById("changeColor").addEventListener('click', function() {
  var color = document.getElementById("backgroundColor").value;
  var body = document.getElementById("body");
  console.debug(body);
  body.style.background = color;
});

function validateForm() {
  var name = document.getElementById('nameInput').value;
  var author = document.getElementById('authorInput').value;
  var yearpublished = document.getElementById('yearpublishedInput').value;
  var pages = document.getElementById('pagesInput').value;

  if (!name.length || !author.length || !yearpublished.length || !pages.length) {
    alert('fields must not be blank');
  }

  if (isNaN(parseInt(yearpublished)) || typeof parseInt(yearpublished) !== 'number') {
    alert('year published must be a number');
  }
  console.log(name, author, yearpublished, pages);
  return true;
}
